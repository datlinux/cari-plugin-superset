#!/usr/bin/env bash

set -euo pipefail

GH_REPO="https://github.com/apache/superset.git"
#PIP_REPO="https://pypi.org/rss/project/apache-superset/releases.xml"
TOOL_NAME="superset"
TOOL_TEST="superset"

fail() {
  echo -e "cari-$TOOL_NAME: $*"
  exit 1
}

curl_opts=(-fSL#)

if [ -n "${GITHUB_API_TOKEN:-}" ]; then
  curl_opts=("${curl_opts[@]}" -H "Authorization: token $GITHUB_API_TOKEN")
fi

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    LC_ALL=C sort -t. -k 1,1 -k 2,2n -k 3,3n -k 4,4n -k 5,5n | awk '{print $2}'
}

list_github_tags() {
  git ls-remote --tags --refs "$GH_REPO" | grep -o 'refs/tags/.*' | \
  cut -d/ -f3- | sed 's/^v//' | grep -e "^[0-9]\{1,2\}\.[0-9]\{1,2\}\.[0-9]\{1,2\}$"
}

#list_pip_tags() {
#  curl -s $PIP_REPO | sed -n 's/\s*<title>\([0-9]*\)/\1/p' | grep -v "PyPI recent" |  cut -d "<" -f1
#}

list_all_versions() {
  list_github_tags
  #list_pip_tags
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"

  if [ "$install_type" != "version" ]; then
    fail "cari-$TOOL_NAME supports release installs only!"
  fi

  (
    #rm -rf "$CARI_VENV_PATH/superset/$version"
    mkdir -p "$CARI_VENV_PATH/superset/$version"
    if [ ! -d $HOME/tmp ]; then
        mkdir $HOME/tmp
    fi

    if [ ! -f "$HOME/.pyenv/versions/3.9.13/bin/python3.9" ]; then
        echo "
    ⚠️  Apache Superset has compatibility issues with Python-3.10. ( Please check 
    the DAT Linux announcements page for any updates on this issue: 
    https://github.com/dat-linux/community/discussions/categories/announcements )

    In order to proceed with Superset, Python-3.9 must be installed concurrently.
    "
        read -p "Proceed with Python-3.9 install? [Y|n]: " input
        if [[ $input != "Y" && $input != "y" && $input != "" ]]; then
            exit 0
        fi
        if [ ! -f $HOME/.pyenv/bin/pyenv ]; then
            git clone https://github.com/pyenv/pyenv.git $HOME/.pyenv
        fi
        echo -e "\e[0;36mInstalling Python-3.9, please be patient.. \e[0;m"
        $HOME/.pyenv/bin/pyenv install 3.9.13
    fi

    cd "$CARI_VENV_PATH/superset/$version"
    $HOME/.pyenv/bin/pyenv local 3.9.13
    $HOME/.pyenv/versions/3.9.13/bin/python3 -m venv "$CARI_VENV_PATH/superset/$version"
    source "$CARI_VENV_PATH/superset/$version/bin/activate"
    if [ -z $CARI_PIP_CACHE/3.9.13 ]; then
      CARI_PIP_CACHE="$HOME/.pip-cache"
    fi
    mkdir -p "$CARI_PIP_CACHE/3.9.13"

    $CARI_VENV_PATH/superset/$version/bin/python3 -m pip install --upgrade pip
    $CARI_VENV_PATH/superset/$version/bin/python3 -m pip install --cache-dir="$CARI_PIP_CACHE/3.9.13" wheel
    $CARI_VENV_PATH/superset/$version/bin/python3 -m pip install --cache-dir="$CARI_PIP_CACHE/3.9.13" pillow
    $CARI_VENV_PATH/superset/$version/bin/python3 -m pip install --cache-dir="$CARI_PIP_CACHE/3.9.13" image
    wget -O- https://raw.githubusercontent.com/apache/superset/$version/requirements/base.txt | tail -n +10 > $install_path/superset_requirements.txt
    $CARI_VENV_PATH/superset/$version/bin/python3 -m pip install --cache-dir="$CARI_PIP_CACHE/3.9.13" apache-superset==$version -r $install_path/superset_requirements.txt

    echo -e "\e[0;36mRunning: export FLASK_APP=$CARI_VENV_PATH/superset/$version/bin/superset\e[0;m"
    export FLASK_APP=$CARI_VENV_PATH/superset/$version/bin/superset

    if [ ! -x /usr/bin/openssl ]; then
      sudo apt install openssl
    fi
    SECRET_KEY="`openssl rand -base64 42`"
    echo "SECRET_KEY=\"$SECRET_KEY\"" >> "$CARI_VENV_PATH/superset/$version/lib/python3.9/site-packages/superset_config.py"

    echo -e "\e[0;36mRunning: $CARI_VENV_PATH/superset/$version/bin/superset db upgrade\e[0;m"
    $CARI_VENV_PATH/superset/$version/bin/superset db upgrade

    admin_user="`$CARI_VENV_PATH/superset/$version/bin/superset fab list-users | grep -e username:admin`"
    if [[ "$admin_user" == *"Admin"* ]]; then
      echo "Admin user already exists.."
    else 
      echo -e "\e[0;36mPlease follow prompts for admin user creation..\e[0;m"
      echo -e "\e[0;36mRunning: $CARI_VENV_PATH/superset/$version/bin/superset fab create-admin\e[0;m"
      $CARI_VENV_PATH/superset/$version/bin/superset fab create-admin
    fi

    echo -e "\e[0;36mInstall example data and dashboards?\e[0;m"
    read -p "[Y|n]: " input
    if [[ $input == "Y" || $input == "y" || $input == "" ]]; then
        echo "Installing example data and dashboards.."
        $CARI_VENV_PATH/superset/$version/bin/superset load_examples
    fi

    echo -e "\e[0;36mInit..\e[0;m"
    $CARI_VENV_PATH/superset/$version/bin/superset init

    echo -e "\e[0;36mVirtual env:: $CARI_VENV_PATH/superset/$version\e[0;m"

    deactivate
    rm -rf $HOME/tmp

    mkdir -p "$install_path/bin"
    touch "$install_path/bin/superset"
    echo "
    source $CARI_VENV_PATH/superset/$version/bin/activate
    $CARI_VENV_PATH/superset/$version/bin/superset run -p 8088 --with-threads --reload --debugger
    deactivate
    " >> "$install_path/bin/superset"
    chmod a+x "$install_path/bin/superset"
    
    test -x "$CARI_VENV_PATH/superset/$version/bin/$TOOL_TEST" || fail "Expected $install_path/bin/$TOOL_TEST to be executable."
    echo "$TOOL_NAME $version installation was successful!"
  ) || (
    rm -rf "$install_path"
    fail "An error ocurred while installing $TOOL_NAME $version."
  )
}

post_install() {
  rm -rf "$CARI_DOWNLOAD_PATH/"
}
